﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TechRoll : MonoBehaviour
{
    [SerializeField] float rollSpeed = 5.0f;
    Animator anim;
    bool rolling = false;
    const string rollForwardTrigger = "Roll_Forward";
    const string rollBackwardTrigger = "Roll_Backward";

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void TechRollRight(PlayerState _state)
    {
        if (!rolling)
        {
            if (_state.FacingRight)
            {
                if (!_state.GetState(PLAYER_STATE.GETUP))   // Assure trigger only gets called once
                {
                    anim.SetTrigger(rollForwardTrigger);
                    _state.SetState(PLAYER_STATE.GETUP, true);
                }
            }
            else
            {
                if (!_state.GetState(PLAYER_STATE.GETUP))
                {
                    anim.SetTrigger(rollBackwardTrigger);
                    _state.SetState(PLAYER_STATE.GETUP, true);
                }
            }
            StartCoroutine(Roll(_state));
        }
    }

    public void TechRollLeft(PlayerState _state)
    {
        if (!rolling)
        {
            if (_state.FacingRight)
            {
                if (!_state.GetState(PLAYER_STATE.GETUP))
                {
                    anim.SetTrigger(rollBackwardTrigger);
                    _state.SetState(PLAYER_STATE.GETUP, true);
                }
            }
            else
            {
                if (!_state.GetState(PLAYER_STATE.GETUP))
                {
                    anim.SetTrigger(rollForwardTrigger);
                    _state.SetState(PLAYER_STATE.GETUP, true);
                }
            }
            StartCoroutine(Roll(_state, false));
        }
    }

    IEnumerator Roll(PlayerState _state, bool _rollRight = true)
    {
        rolling = true;
        if (_rollRight)
            rollSpeed = Mathf.Abs(rollSpeed);
        else
            rollSpeed = -Mathf.Abs(rollSpeed);


        while (_state.GetState(PLAYER_STATE.KNOCKED_DOWN))
        {
            bool wallCollision = _state.GetState(PLAYER_STATE.WALL_COLLISION_LEFT) || _state.GetState(PLAYER_STATE.WALL_COLLISION_RIGHT);

            if (!wallCollision)
            {
                Vector2 pos = transform.position;
                pos.x += rollSpeed * Time.deltaTime;
                transform.position = pos;
            }
            yield return null;
        }
        rolling = false;
    }
}
