﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
    [SerializeField] GameObject projectilePrefab = null;
    [SerializeField] Transform spawnPoint = null;

    bool facingRight = true;
    bool playerOne = true;

    public void SetupProjectile(PlayerState _state)
    {
        facingRight = _state.FacingRight;
        playerOne = _state.FacingRight;
    }

    public void SpawnProjectile()
    {
        GameObject projectile = Instantiate(projectilePrefab, spawnPoint.position, spawnPoint.rotation);
        Projectile projectileScript = projectile.GetComponent<Projectile>();
        projectileScript.MoveRight(facingRight);
        projectileScript.IsPlayerOne(playerOne);
    }
}
