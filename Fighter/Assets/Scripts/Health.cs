﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    [Range(1, 340)] [SerializeField] int health = 170;
    [SerializeField] Slider healthSlider = null;

    void Start()
    {
        healthSlider.maxValue = health;
        healthSlider.value = health;
    }

    public int GetHealth()
    {
        return health;
    }

    public void Damage(int _damage)
    {
        health -= _damage;
        if (health <= 0)
            health = 0;
        healthSlider.value = health;
    }
}
