﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    [SerializeField] TMP_Text timerText = null;
    [SerializeField] float timer = 60.0f;
    // bool roundStart = false;
    // int playerOneRoundCount = 0;
    // int playerTwoRoundCount = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0.0f)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            timer = 0.0f;
        }
        timerText.text = Mathf.CeilToInt(timer).ToString();
        if(Input.GetKeyDown(KeyCode.F5))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }
}
