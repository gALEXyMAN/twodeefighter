using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HitBox
{
    public string index;
    public GameObject hitBox;
    public AttackMovement attackMove { get { return hitBox.GetComponent<AttackMovement>(); } }
}

public class HitBoxManager : MonoBehaviour
{
    [SerializeField] HitBox[] hitboxes = null;
#if UNITY_EDITOR
    Animator anim;
    void Start()
    { anim = GetComponent<Animator>(); }
#endif

    GameObject GetHitBox(string _index)
    {
        foreach (HitBox hb in hitboxes)
        {
            if (hb.index == _index)
                return hb.hitBox;
        }
        Debug.LogError("Error: Invalid Hitbox id: " + _index);
#if UNITY_EDITOR
        Debug.LogError("Event called from: " + anim.GetCurrentAnimatorClipInfo(0)[0].clip.name);
#endif
        return null;
    }

    // Called in Animation Event
    public void ActivateHitBox(string _index)
    {
        GetHitBox(_index).SetActive(true);
    }

    public void DeactivateHitBox(string _index)
    {
        GetHitBox(_index).SetActive(false);
    }

    public AttackMovement GetAttackMoveParams(string _id)
    {
        for (int i = 0; i < hitboxes.Length; ++i)
        {
            if (hitboxes[i].index == _id)
                return hitboxes[i].attackMove;
        }
        Debug.LogError("Error: Invalid Hitbox id: " + _id);
        return null;
    }
}