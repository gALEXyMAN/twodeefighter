using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CommandList : MonoBehaviour
{
    protected Animator anim;
    protected ProjectileManager projectile;

    void Start()
    {
        anim = GetComponent<Animator>();
        projectile = GetComponent<ProjectileManager>();
    }

    public abstract void KiCharge(PlayerState _state, InputState _input);
    public abstract void LeftAndRightPunchCommands(PlayerState _state, InputState _input);
    public abstract void LeftAndRightKickCommands(PlayerState _state, InputState _input);
    public abstract void LeftPunchAndKickCommands(PlayerState _state, InputState _input);
    public abstract void RightPunchAndKickCommands(PlayerState _state, InputState _input);
    public abstract void LeftPunchCommands(PlayerState _state, InputState _input);
    public abstract void RightPunchCommands(PlayerState _state, InputState _input);
    public abstract void LeftKickCommands(PlayerState _state, InputState _input);
    public abstract void RightKickCommands(PlayerState _state, InputState _input);
}