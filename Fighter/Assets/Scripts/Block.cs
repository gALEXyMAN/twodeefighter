﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public void UpdateBlock(ref PlayerState _state, InputState _input)
    {
        if (!_state.GetState(PLAYER_STATE.ATTACKING))
        {
            if (_state.FacingRight)
            {
                if (_input.GetInput(INPUT.LEFT))
                {
                    _state.SetState(PLAYER_STATE.BLOCKING, true);
                }
                else
                {
                    _state.SetState(PLAYER_STATE.BLOCKING, false);
                }
            }
            else
            {
                if (_input.GetInput(INPUT.RIGHT))
                {
                    _state.SetState(PLAYER_STATE.BLOCKING, true);
                }
                else
                {
                    _state.SetState(PLAYER_STATE.BLOCKING, false);
                }
            }
        }
    }
}
