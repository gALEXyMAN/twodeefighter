﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float speed = 1.0f;
     [Header("When facing right")]
    [SerializeField] Vector2 pushBackForce = Vector2.zero;
    [SerializeField] int pushBackLengthInFrames = 3;
    [SerializeField] int damage = 10;
    Animator anim;
    BoxCollider2D boxCollider;
    bool moveRight = true;
    bool playerOneProjectile = true;
    const string destroyTrigger = "Destroy";

    public void MoveRight(bool _right)
    {
        moveRight = _right;
        GetComponent<SpriteRenderer>().flipX = !_right;
    }

    public void IsPlayerOne(bool _p1)
    {
        playerOneProjectile = _p1;
    }

    void Start()
    {
        anim = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        if (moveRight)
        {
            pos.x += speed * Time.deltaTime;
        }
        else
        {
            pos.x -= speed * Time.deltaTime;

        }
        transform.position = pos;

        Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, boxCollider.size, 0);
        foreach (Collider2D hit in hits)
        {
            if (hit == boxCollider)
                continue;

            if ((playerOneProjectile && hit.tag == "HurtBoxP2") ||
            (!playerOneProjectile && hit.tag == "HurtBoxP1"))
            {
                hit.GetComponent<HitStun>().HitHigh(pushBackForce, pushBackLengthInFrames, damage);
                anim.SetTrigger(destroyTrigger);
            }
        }
    }
    public void DestroyProjectile()
    {
        Destroy(gameObject);
    }
}
