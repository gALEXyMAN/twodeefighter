using System.Collections;
using System.Collections.Generic;

public enum PLAYER_STATE
{
    ATTACKING,
    BLOCKING,
    CROUCHING,
    JUMPING,
    FALLING,
    MOVING,
    DASHING,
    KNOCKED_DOWN,
    RECOVERING,
    WAVEDASH,
    CROUCHDASH,
    CHARGE_HORIZONTAL,
    CHARGE_VERTICAL,
    SIDESTEP_STARTUP,
    SIDESTEPPING,
    WALL_COLLISION_LEFT,    // Back
    WALL_COLLISION_RIGHT,
    OPPONENT_WALL_COLLISION,   // Back COLLIDER
    HIT,
    GETUP,
    INVULNERABLE,
    MAX_STATE
}

public class PlayerState
{
    bool[] playerStates = new bool[(int)PLAYER_STATE.MAX_STATE];
    bool facingRight = true;
    public bool FacingRight { get { return facingRight; } set { facingRight = value; } }
    bool playerOne = true;
    public bool PlayerOne { get { return playerOne; } set { playerOne = value; } }

    public bool GetState(params PLAYER_STATE[] _states)
    {
        foreach (PLAYER_STATE state in _states)
        {
            if (playerStates[(int)state] == false) return false;
        }
        return true;
    }

    public void SetState(PLAYER_STATE _state, bool _value)
    {
        playerStates[(int)_state] = _value;
    }
}