using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler
{
    public void UpdateInput(ref PlayerState _state, InputState _inputState, bool _playerOne)
    {
        if (_playerOne)
        {
            if (Input.GetButton("Left Punch"))
            {
                _inputState.SetInput(INPUT.LEFT_PUNCH, true);
            }
            else
            {
                _inputState.SetInput(INPUT.LEFT_PUNCH, false);
            }

            if (Input.GetButton("Right Punch")) _inputState.SetInput(INPUT.RIGHT_PUNCH, true);
            else _inputState.SetInput(INPUT.RIGHT_PUNCH, false);

            if (Input.GetButton("Left Kick")) _inputState.SetInput(INPUT.LEFT_KICK, true);
            else _inputState.SetInput(INPUT.LEFT_KICK, false);

            if (Input.GetButton("Right Kick")) _inputState.SetInput(INPUT.RIGHT_KICK, true);
            else _inputState.SetInput(INPUT.RIGHT_KICK, false);

            if (Input.GetAxisRaw("Vertical") < 0)
            {
                _inputState.SetInput(INPUT.DOWN, true);
                _inputState.SetInput(INPUT.UP, false);
                //_state.SetState(PLAYER_STATE.CROUCHING, true);
            }
            else if (Input.GetAxisRaw("Vertical") > 0)
            {
                _inputState.SetInput(INPUT.DOWN, false);
                _inputState.SetInput(INPUT.UP, true);
                _state.SetState(PLAYER_STATE.CROUCHING, false);
            }
            else
            {
                _inputState.SetInput(INPUT.UP, false);
                _inputState.SetInput(INPUT.DOWN, false);
                _state.SetState(PLAYER_STATE.CROUCHING, false);
            }

            if (Input.GetAxisRaw("Horizontal") < 0)
            {
                _inputState.SetInput(INPUT.LEFT, true);
                _inputState.SetInput(INPUT.RIGHT, false);
            }
            else if (Input.GetAxisRaw("Horizontal") > 0)
            {
                // Move right
                _inputState.SetInput(INPUT.LEFT, false);
                _inputState.SetInput(INPUT.RIGHT, true);
            }
            else
            {
                _inputState.SetInput(INPUT.LEFT, false);
                _inputState.SetInput(INPUT.RIGHT, false);
            }
        }
        else
        {
            if (Input.GetButton("Left Punch2"))
            {
                _inputState.SetInput(INPUT.LEFT_PUNCH, true);
            }
            else
            {
                _inputState.SetInput(INPUT.LEFT_PUNCH, false);
            }

            if (Input.GetButton("Right Punch2")) _inputState.SetInput(INPUT.RIGHT_PUNCH, true);
            else _inputState.SetInput(INPUT.RIGHT_PUNCH, false);

            if (Input.GetButton("Left Kick2")) _inputState.SetInput(INPUT.LEFT_KICK, true);
            else _inputState.SetInput(INPUT.LEFT_KICK, false);

            if (Input.GetButton("Right Kick2")) _inputState.SetInput(INPUT.RIGHT_KICK, true);
            else _inputState.SetInput(INPUT.RIGHT_KICK, false);

            if (Input.GetAxisRaw("Vertical2") < 0)
            {
                _inputState.SetInput(INPUT.DOWN, true);
                _inputState.SetInput(INPUT.UP, false);
                //_state.SetState(PLAYER_STATE.CROUCHING, true);
            }
            else if (Input.GetAxisRaw("Vertical2") > 0)
            {
                _inputState.SetInput(INPUT.DOWN, false);
                _inputState.SetInput(INPUT.UP, true);
                _state.SetState(PLAYER_STATE.CROUCHING, false);
            }
            else
            {
                _inputState.SetInput(INPUT.UP, false);
                _inputState.SetInput(INPUT.DOWN, false);
                _state.SetState(PLAYER_STATE.CROUCHING, false);
            }

            if (Input.GetAxisRaw("Horizontal2") < 0)
            {
                _inputState.SetInput(INPUT.LEFT, true);
                _inputState.SetInput(INPUT.RIGHT, false);
            }
            else if (Input.GetAxisRaw("Horizontal2") > 0)
            {
                // Move right
                _inputState.SetInput(INPUT.LEFT, false);
                _inputState.SetInput(INPUT.RIGHT, true);
            }
            else
            {
                _inputState.SetInput(INPUT.LEFT, false);
                _inputState.SetInput(INPUT.RIGHT, false);
            }
        }
    }
}