﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackMovement : MonoBehaviour
{
    public Vector2 force = new Vector2(10.0f, 0.0f);
    public int length = 3;    // in frames
}
