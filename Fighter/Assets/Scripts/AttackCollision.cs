﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCollision : MonoBehaviour
{
    enum MIXUP
    {
        HIGH,
        MID,
        LOW,
        LAUNCH
    }

    [SerializeField] MIXUP mixup = MIXUP.HIGH;
    [Header("When facing right")]
    [SerializeField] Vector2 pushBackForce = Vector2.zero;
    [SerializeField] int pushBackLengthInFrames = 3;
    [SerializeField] int damage = 10;

    BoxCollider2D[] boxColliders;
    string hitBoxTag = "";

    void Awake()
    {
        gameObject.SetActive(false);
        boxColliders = GetComponents<BoxCollider2D>();
        hitBoxTag = transform.parent.tag;
    }

    void OnEnable()
    {
        bool hitCollision = false;
        foreach (BoxCollider2D collider in boxColliders)
        {
            if(hitCollision) break;
            Vector2 pos = transform.position;
            float direction = transform.parent.transform.localScale.x > 0.0f ? 1.0f : -1.0f;    // negate offset when facing left (2p side)
            Collider2D[] hits = Physics2D.OverlapBoxAll(pos + (collider.offset * direction), collider.size, 0);

            foreach (Collider2D hit in hits)
            {
                if (hit == collider)
                    continue;

                if ((hitBoxTag == "PlayerOne" && hit.tag == "HurtboxP2") ||
                    (hitBoxTag == "PlayerTwo" && hit.tag == "HurtboxP1"))
                {
                    switch (mixup)
                    {
                        case MIXUP.HIGH: hit.GetComponent<Hurtbox>().HitHigh(pushBackForce, pushBackLengthInFrames, damage); break;
                        case MIXUP.MID: hit.GetComponent<Hurtbox>().HitMid(pushBackForce, pushBackLengthInFrames, damage); break;
                        case MIXUP.LOW: hit.GetComponent<Hurtbox>().HitLow(pushBackForce, pushBackLengthInFrames, damage); break;
                        case MIXUP.LAUNCH: hit.GetComponent<Hurtbox>().Launched(pushBackForce, pushBackLengthInFrames, damage); break;
                        default: break;
                    }
                    hitCollision = true;
                    break;
                }
            }
        }
    }
}
