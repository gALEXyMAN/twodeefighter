using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeInput : MonoBehaviour
{
    [SerializeField] int bufferForwardTimeInFrames = 3;
    [SerializeField] int chargeTimeInFrames = 30;
    [SerializeField] int openWindowInFrames = 3;
    int startFrame = 0;
    int startFrameVertical = 0;
    int releaseFrame = 0;
    int releaseFrameVertical = 0;
    int activeFrame = 0;
    bool chargingLeft = false;
    bool chargingRight = false;
    bool chargingDown = false;
    bool holdLeft = false;
    bool holdRight = false;
    bool holdDown = false;
    bool sonicBoom = false;

    public void UpdateChargeInput(ref PlayerState _state, InputState _input)
    {
        if (_input.GetInput(INPUT.LEFT))
        {
            holdLeft = true;
            if(chargingRight && startFrame < Time.frameCount - chargeTimeInFrames)
            {
                //Debug.Log("Sonic bool1 left");
                sonicBoom = true;
                _state.SetState(PLAYER_STATE.CHARGE_HORIZONTAL, true);
                activeFrame = Time.frameCount;
            }
            if (!chargingLeft)
            {
                startFrame = Time.frameCount;
                chargingLeft = true;
            }
            chargingRight = false;
        }
        else if (_input.GetInput(INPUT.LEFT) == false)
        {
            if (holdLeft)
            {
                releaseFrame = Time.frameCount;
                holdLeft = false;
            }
            if (releaseFrame < Time.frameCount - bufferForwardTimeInFrames) chargingLeft = false;
        }

        if (_input.GetInput(INPUT.RIGHT))
        {
            holdRight = true;
            if (chargingLeft && startFrame < Time.frameCount - chargeTimeInFrames)
            {
                //Debug.Log("Sonic boom!");
                sonicBoom = true;
                _state.SetState(PLAYER_STATE.CHARGE_HORIZONTAL, true);
                activeFrame = Time.frameCount;
            }
            if (!chargingRight)
            {
                startFrame = Time.frameCount;
                chargingRight = true;
            }
            chargingLeft = false;
        }
        else if(_input.GetInput(INPUT.RIGHT) == false)
        {
            if(holdRight)
            {
                releaseFrame = Time.frameCount;
                holdRight = false;
            }
            if(releaseFrame < Time.frameCount - bufferForwardTimeInFrames) chargingRight = false;
        }

        if (_input.GetInput(INPUT.DOWN))
        {
            holdDown = true;
            if (!chargingDown)
            {
                startFrameVertical = Time.frameCount;
                chargingDown = true;
            }
        }
        else if (_input.GetInput(INPUT.DOWN) == false)
        {
            if(holdDown)
            {
                holdDown = false;
                releaseFrameVertical = Time.frameCount;
            }
            if (releaseFrameVertical < Time.frameCount - bufferForwardTimeInFrames) chargingDown = false;
        }

        if (_input.GetInput(INPUT.UP))
        {
            if (chargingDown && startFrameVertical < Time.frameCount - chargeTimeInFrames)
            {
                Debug.Log("Flash Kick!");
                chargingDown = false;
                _state.SetState(PLAYER_STATE.CHARGE_VERTICAL, true);
                activeFrame = Time.frameCount;
            }
        }

        if(sonicBoom)
        {
            if(activeFrame < Time.frameCount - openWindowInFrames)
            {
                activeFrame = 0;
                _state.SetState(PLAYER_STATE.CHARGE_HORIZONTAL, false);
                _state.SetState(PLAYER_STATE.CHARGE_VERTICAL, false);
                sonicBoom = false;
            }
        }
    }
}