using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrouchDash : MonoBehaviour
{
    [SerializeField] int windowBetweenInputs = 10;
    [SerializeField] int openWindowInFrames = 6;
    bool leftDash = false;
    bool rightDash = false;

    string sequence = "";
    bool leftPressed = false;
    bool rightPressed = false;
    bool downPressed = false;
    bool downRight = false;
    bool downLeft = false;
    int lastFramePressed = 0;
    int activeFrame = 0;

    public bool DashLeft()
    {
        return leftDash;
    }

    public bool DashRight()
    {
        return rightDash;
    }

    public void UpdateCrouchDash(ref PlayerState _state, InputState _input)
    {
        if (_input.GetInput(INPUT.DOWN))
        {
            if (!downPressed)
            {
                downPressed = true;
                lastFramePressed = Time.frameCount;
                sequence += "D ";
            }
        }
        else if (_input.GetInput(INPUT.DOWN) == false)
        {
            if (downPressed)
            {
                downPressed = false;
            }
        }

        if (_input.GetInput(INPUT.RIGHT) && _input.GetInput(INPUT.DOWN))
        {
            if (!downRight)
            {
                downRight = true;
                sequence += "DR ";
                lastFramePressed = Time.frameCount;
            }
        }
        else
        {
            downRight = false;
        }

        if (_input.GetInput(INPUT.LEFT) && _input.GetInput(INPUT.DOWN))
        {
            if (!downLeft)
            {
                downLeft = true;
                sequence += "DL ";
                lastFramePressed = Time.frameCount;
            }
        }
        else
        {
            downLeft = false;
        }

        if (_input.GetInput(INPUT.RIGHT))
        {
            if (!rightPressed)
            {
                rightPressed = true;
                lastFramePressed = Time.frameCount;
                sequence += "R ";
            }
        }
        else if (_input.GetInput(INPUT.RIGHT) == false)
        {
            if (rightPressed)
            {
                rightPressed = false;
            }
        }

        if (_input.GetInput(INPUT.LEFT))
        {
            if (!leftPressed)
            {
                leftPressed = true;
                lastFramePressed = Time.frameCount;
                sequence += "L ";
            }
        }
        else if (_input.GetInput(INPUT.LEFT) == false)
        {
            if (leftPressed)
            {
                leftPressed = false;
            }
        }

        if (lastFramePressed < Time.frameCount - windowBetweenInputs)
        {
            sequence = "";
        }

        if (sequence.Contains("D DR R "))
        {
            //Debug.Log("Fireball right");
            rightDash = true;
            sequence = "";
            activeFrame = Time.frameCount;
            _state.SetState(PLAYER_STATE.CROUCHDASH, true);
        }

        if (sequence.Contains("D DL L "))
        {
            //Debug.Log("fireball left");
            leftDash = true;
            sequence = "";
            activeFrame = Time.frameCount;
            _state.SetState(PLAYER_STATE.CROUCHDASH, true);
        }

        if (leftDash || rightDash)
        {
            if(activeFrame < Time.frameCount - openWindowInFrames)
            {
                activeFrame = 0;
                leftDash = false;
                rightDash = false;
                _state.SetState(PLAYER_STATE.CROUCHDASH, false);
            }
        }
    }
}