﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hurtbox : MonoBehaviour
{
    HitStun hit;

    void Start()
    {
        hit = GetComponentInParent<HitStun>();
    }

    public void HitHigh(Vector2 _force, int _length, int _damage)
    {
        hit.HitHigh(_force, _length, _damage);
    }

    public void HitMid(Vector2 _force, int _length, int _damage)
    {
        hit.HitMid(_force, _length, _damage);
    }

    public void HitLow(Vector2 _force, int _length, int _damage)
    {
        hit.HitLow(_force, _length, _damage);
    }

    public void Launched(Vector2 _force, int _length, int _damage)
    {
        hit.Launched(_force, _length, _damage);
    }
}
