﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetNormalizedTime : StateMachineBehaviour
{
    [SerializeField] private string targetParameter = "Normalized Time";

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetFloat(targetParameter, stateInfo.normalizedTime);
    }
}
