using System.Collections;
using System.Collections.Generic;

public enum INPUT
{
    LEFT = 0,
    RIGHT,
    UP,
    DOWN,
    LEFT_PUNCH,
    RIGHT_PUNCH,
    LEFT_KICK,
    RIGHT_KICK,
    MAX_INPUT
}

public class InputState
{
    bool[] inputState = new bool[(int)INPUT.MAX_INPUT];

    public bool[] GetCurrentState() { return inputState; }

    public bool GetInput(params INPUT[] _inputs)
    {
        foreach (INPUT _input in _inputs)
        {
            if (inputState[(int)_input] == false)
            {
                return false;
            }
        }
        return true;
    }

    public void SetInput(INPUT _input, bool _value)
    {
        inputState[(int)_input] = _value;
    }
}