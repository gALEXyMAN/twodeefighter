﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HurtboxTransform
{
    public string name;
    public BoxCollider2D collider;
}

public class HurtboxTransformer : MonoBehaviour
{
    public HurtboxTransform[] hurtboxes;
    new public BoxCollider2D collider;
    bool transforming = false;
    HurtboxTransform activeHurtbox = null;

    HurtboxTransform GetHurtbox(string _name)
    {
        for (int i = 0; i < hurtboxes.Length; ++i)
        {
            if (hurtboxes[i].name == _name)
                return hurtboxes[i];
        }
        Debug.LogError("Hurtbox \"" + _name + "\" doens't exist");
        return null;
    }

    // Called in animation events
    public void ActivateHurtBox()
    {
        collider.gameObject.SetActive(true);
    }

    public void DeactivateHurtBox()
    {
        collider.gameObject.SetActive(false);
    }

    public void TransformHurtbox(string _name)
    {
        activeHurtbox = GetHurtbox(_name);
        transforming = true;
    }

    void Update()
    {
        if (transforming)
        {
            collider.size = Vector2.Lerp(collider.size, activeHurtbox.collider.size, 0.5f);
            collider.offset = Vector2.Lerp(collider.offset, activeHurtbox.collider.offset, 0.5f);
            if (collider.size == activeHurtbox.collider.size && collider.offset == activeHurtbox.collider.offset)
            {
                transforming = false;
            }
        }
    }
}
