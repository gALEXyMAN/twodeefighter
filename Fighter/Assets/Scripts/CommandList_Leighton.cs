﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandList_Leighton : CommandList
{
        const string lpTrigger = "LeftPunch", rpTrigger = "RightPunch", lkTrigger = "LeftKick", rkTrigger = "RightKick";

    public override void KiCharge(PlayerState _state, InputState _input)
    {

    }

    public override void LeftAndRightPunchCommands(PlayerState _state, InputState _input)
    {

    }

    public override void LeftAndRightKickCommands(PlayerState _state, InputState _input)
    {

    }

    public override void LeftPunchAndKickCommands(PlayerState _state, InputState _input)
    {

    }

    public override void RightPunchAndKickCommands(PlayerState _state, InputState _input)
    {

    }

    public override void LeftPunchCommands(PlayerState _state, InputState _input)
    {
        anim.SetTrigger(lpTrigger);
    }

    public override void RightPunchCommands(PlayerState _state, InputState _input)
    {
        if(_state.GetState(PLAYER_STATE.WAVEDASH))
        {
            anim.SetTrigger("Electric");
            return;
        }

        if (_state.GetState(PLAYER_STATE.CHARGE_HORIZONTAL))
        {
            // Charge attack
            anim.SetTrigger("Sonic_Boom");
            projectile.SetupProjectile(_state);
            return;
        }

        if (_input.GetInput(INPUT.LEFT))
        {
            if (_state.FacingRight)
            {
                anim.SetTrigger("b+2");
                return;
            }
        }

        if (_input.GetInput(INPUT.RIGHT))
        {
            if (_state.FacingRight)
            {

            }
            else
            {
                anim.SetTrigger("b+2");
                return;
            }
        }

        anim.SetTrigger(rpTrigger);

    }

    public override void LeftKickCommands(PlayerState _state, InputState _input)
    {
        anim.SetTrigger(lkTrigger);
    }

    public override void RightKickCommands(PlayerState _state, InputState _input)
    {
        anim.SetTrigger(rkTrigger);
    }
}
