using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandList_Fox : CommandList
{
    const string lpTrigger = "LeftPunch", rpTrigger = "RightPunch", lkTrigger = "LeftKick", rkTrigger = "RightKick";

    public override void KiCharge(PlayerState _state, InputState _input)
    {

    }

    public override void LeftAndRightPunchCommands(PlayerState _state, InputState _input)
    {
        if(_input.GetInput(INPUT.DOWN, INPUT.RIGHT))
        {
            if(_state.FacingRight)
            {
                anim.SetTrigger("df1+2");
                return;
            }
        }

        if(_input.GetInput(INPUT.DOWN, INPUT.LEFT))
        {
            if(_state.FacingRight)
            {

            }
            else
            {
                anim.SetTrigger("df1+2");
            }
        }
    }

    public override void LeftAndRightKickCommands(PlayerState _state, InputState _input)
    {

    }

    public override void LeftPunchAndKickCommands(PlayerState _state, InputState _input)
    {

    }

    public override void RightPunchAndKickCommands(PlayerState _state, InputState _input)
    {

    }

    public override void LeftPunchCommands(PlayerState _state, InputState _input)
    {
        if(_input.GetInput(INPUT.LEFT))
        {
            if(_state.FacingRight)
            {
                anim.SetTrigger("b+1");
                return;
            }
        }

        if(_input.GetInput(INPUT.RIGHT))
        {
            if(_state.FacingRight)
            {

            }
            else
            {
                anim.SetTrigger("b+1");
                return;
            }
        }
        anim.SetTrigger(lpTrigger);
    }

    public override void RightPunchCommands(PlayerState _state, InputState _input)
    {
        if(_state.GetState(PLAYER_STATE.WAVEDASH) && !_state.GetState(PLAYER_STATE.DASHING))
        {
            anim.SetTrigger("Electric");
            return;
        }

        if (_state.GetState(PLAYER_STATE.CHARGE_HORIZONTAL))
        {
            // Charge attack
            anim.SetTrigger("Sonic_Boom");
            projectile.SetupProjectile(_state);
            return;
        }

        if (_input.GetInput(INPUT.LEFT))
        {
            if (_state.FacingRight)
            {
                anim.SetTrigger("b+2");
                return;
            }
        }

        if (_input.GetInput(INPUT.RIGHT))
        {
            if (_state.FacingRight)
            {

            }
            else
            {
                anim.SetTrigger("b+2");
                return;
            }
        }

        anim.SetTrigger(rpTrigger);

    }

    public override void LeftKickCommands(PlayerState _state, InputState _input)
    {
        anim.SetTrigger(lkTrigger);
    }

    public override void RightKickCommands(PlayerState _state, InputState _input)
    {
        if(_state.GetState(PLAYER_STATE.CROUCHDASH))
        {
            anim.SetTrigger("qcf+4");
            return;
        }
        anim.SetTrigger(rkTrigger);
    }
}