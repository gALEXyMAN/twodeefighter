using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wavedash : MonoBehaviour
{
    [SerializeField] int windowBetweenInputs = 10;
    [SerializeField] float dashAmount = 10;
    [SerializeField] int dashLengthInFrames = 13;
    const string waveDashTrigger = "WaveDash";
    bool leftDash = false;
    bool rightDash = false;

    string sequence = "";
    bool leftPressed = false;
    bool rightPressed = false;
    bool downPressed = false;
    bool downRight = false;
    bool downLeft = false;
    int lastFramePressed = 0;
    int startFrame = 0;
    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public bool DashLeft()
    {
        return leftDash;
    }

    public bool DashRight()
    {
        return rightDash;
    }

    public void UpdateWaveDash(ref PlayerState _state, InputState _input)
    {
        //_state.SetState(PLAYER_STATE.WAVEDASH, leftDash || rightDash ? true : false);

        if (_input.GetInput(INPUT.DOWN))
        {
            if (!downPressed)
            {
                downPressed = true;
                lastFramePressed = Time.frameCount;
                sequence += "D ";
            }
        }
        else if (_input.GetInput(INPUT.DOWN) == false)
        {
            if (downPressed)
            {
                downPressed = false;
            }
        }

        if (_input.GetInput(INPUT.RIGHT) && _input.GetInput(INPUT.DOWN))
        {
            if (!downRight)
            {
                downRight = true;
                sequence += "DR ";
                lastFramePressed = Time.frameCount;
            }
        }
        else
        {
            downRight = false;
        }

        if (_input.GetInput(INPUT.LEFT) && _input.GetInput(INPUT.DOWN))
        {
            if (!downLeft)
            {
                downLeft = true;
                sequence += "DL ";
                lastFramePressed = Time.frameCount;
            }
        }
        else
        {
            downLeft = false;
        }

        if (_input.GetInput(INPUT.RIGHT))
        {
            if (!rightPressed)
            {
                rightPressed = true;
                lastFramePressed = Time.frameCount;
                sequence += "R ";
            }
        }
        else if (_input.GetInput(INPUT.RIGHT) == false)
        {
            if (rightPressed)
            {
                rightPressed = false;
                if (!downPressed)
                {
                    sequence += "n ";
                }
            }
        }

        if (_input.GetInput(INPUT.LEFT))
        {
            if (!leftPressed)
            {
                leftPressed = true;
                lastFramePressed = Time.frameCount;
                sequence += "L ";

            }
        }
        else if (_input.GetInput(INPUT.LEFT) == false)
        {
            if (leftPressed)
            {
                leftPressed = false;
                if (!downPressed)
                {
                    sequence += "n ";
                }
            }
        }

        if (lastFramePressed < Time.frameCount - windowBetweenInputs)
        {
            sequence = "";
        }

        if (sequence.Contains("R n D DR ") || sequence.Contains("R n D DR R "))
        {
            //Debug.Log("Wavedash right");
            rightDash = true;
            sequence = "";
            _state.SetState(PLAYER_STATE.WAVEDASH, true);
            if (_state.FacingRight)
            {
                anim.SetTrigger(waveDashTrigger);
                startFrame = Time.frameCount;
                StartCoroutine(Dash(_state));
            }
        }
        else
        {
            //_state.SetState(PLAYER_STATE.WAVEDASH, false);
            rightDash = false;
        }

        if (sequence.Contains("L n D DL ") || sequence.Contains("L n D DL L "))
        {
            Debug.Log("Wavedash left");
            leftDash = true;
            sequence = "";
            _state.SetState(PLAYER_STATE.WAVEDASH, true);
            if (!_state.FacingRight)
            {
                anim.SetTrigger(waveDashTrigger);
                startFrame = Time.frameCount;
                StartCoroutine(Dash(_state));
            }
        }
        else
        {
            // _state.SetState(PLAYER_STATE.WAVEDASH, false);
            leftDash = false;
        }
    }

    IEnumerator Dash(PlayerState _state)
    {
        if (_state.FacingRight)
        {
            while (Time.frameCount < startFrame + dashLengthInFrames)
            {
                if (!_state.GetState(PLAYER_STATE.WALL_COLLISION_RIGHT) &&
                    !_state.GetState(PLAYER_STATE.OPPONENT_WALL_COLLISION))
                {
                    Vector2 pos = transform.position;
                    pos.x += dashAmount * Time.deltaTime;
                    transform.position = pos;
                }
                yield return null;
            }
        }
        else
        {
            while (Time.frameCount < startFrame + dashLengthInFrames)
            {
                if (!_state.GetState(PLAYER_STATE.WALL_COLLISION_LEFT) &&
                    !_state.GetState(PLAYER_STATE.OPPONENT_WALL_COLLISION))
                {
                    Vector2 pos = transform.position;
                    pos.x -= dashAmount * Time.deltaTime;
                    transform.position = pos;
                }
                yield return null;
            }
        }
    }
}