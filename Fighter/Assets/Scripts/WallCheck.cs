﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCheck : MonoBehaviour
{
    [SerializeField] BoxCollider2D leftCollider = null;
    [SerializeField] BoxCollider2D rightCollider = null;

    public void UpdateCollisionCheck(PlayerState _state)
    {
        Collider2D[] hitsLeft = Physics2D.OverlapBoxAll(leftCollider.transform.position, leftCollider.size, 0);
        Collider2D[] hitsRight = Physics2D.OverlapBoxAll(rightCollider.transform.position, rightCollider.size, 0);
        
        _state.SetState(PLAYER_STATE.WALL_COLLISION_LEFT, false);
        _state.SetState(PLAYER_STATE.WALL_COLLISION_RIGHT, false);

        foreach (Collider2D hit in hitsLeft)
        {
            if (hit == leftCollider)
                continue;

            if (hit.tag == "Wall")
            {
                _state.SetState(PLAYER_STATE.WALL_COLLISION_LEFT, true);
                return;
            }
        }
        foreach(Collider2D hit in hitsRight)
        {
            if(hit == rightCollider)
                continue;

            if(hit.tag == "Wall")
            {
                _state.SetState(PLAYER_STATE.WALL_COLLISION_RIGHT, true);
            }
        }
    }
}
