using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    PlayerState playerState;
    InputState inputState;
    InputHandler input;
    Movement movement;
    Dash dash;
    Wavedash waveDash;
    CrouchDash crouchDash;
    ChargeInput charge;
    Attack attack;
    WallCheck wallCheck;
    MovementCollision moveCollision;
    Block block;
    HitStun hitStun;

    bool playerOne = true;
    GameObject opponent;
    SpriteRenderer sprite;
    Animator anim;

    void Start()
    {
        playerState = new PlayerState();
        inputState = new InputState();
        input = new InputHandler();
        movement = GetComponent<Movement>();
        dash = GetComponent<Dash>();
        waveDash = GetComponent<Wavedash>();
        crouchDash = GetComponent<CrouchDash>();
        charge = GetComponent<ChargeInput>();
        attack = GetComponent<Attack>();
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        wallCheck = GetComponent<WallCheck>();
        moveCollision = GetComponent<MovementCollision>();
        block = GetComponent<Block>();
        hitStun = GetComponent<HitStun>();

        hitStun.SetState(playerState);

        if (tag == "PlayerOne")
        {
            playerOne = true;
            opponent = GameObject.FindWithTag("PlayerTwo");
        }
        else if (tag == "PlayerTwo")
        {
            playerOne = false;
            opponent = GameObject.FindWithTag("PlayerOne");
        }
        else Debug.LogWarning("Player tag not set!");

        playerState.FacingRight = playerOne;
        playerState.PlayerOne = playerOne;
    }

    void Update()
    {
        playerState.FacingRight = opponent.transform.position.x > transform.position.x ? true : false;
        Vector3 scale = transform.localScale;
        scale.x = playerState.FacingRight ? Mathf.Abs(scale.x) : -Mathf.Abs(scale.x);
        transform.localScale = scale;

        anim.SetBool("KnockedDown", playerState.GetState(PLAYER_STATE.KNOCKED_DOWN));
        anim.SetBool("Attacking", playerState.GetState(PLAYER_STATE.ATTACKING));

        input.UpdateInput(ref playerState, inputState, playerOne);
        movement.UpdateMovement(ref playerState, inputState);
        if (waveDash) waveDash.UpdateWaveDash(ref playerState, inputState);
        if (crouchDash) crouchDash.UpdateCrouchDash(ref playerState, inputState);
        if (charge) charge.UpdateChargeInput(ref playerState, inputState);
        if (dash) dash.UpdateDash(ref playerState, inputState);
        attack.UpdateAttack(ref playerState, inputState);
        block.UpdateBlock(ref playerState, inputState);

        moveCollision.UpdateMovementCollision(playerState);
        wallCheck.UpdateCollisionCheck(playerState);
    }

    public bool IsFacingRight()
    {
        return playerState.FacingRight;
    }

    // Animation Events
    public void StartAttack()
    {
        playerState.SetState(PLAYER_STATE.ATTACKING, true);
    }

    public void EndAttack()
    {
        playerState.SetState(PLAYER_STATE.ATTACKING, false);
        ResetTriggers();
    }

    public void ResetTriggers()
    {
        foreach (AnimatorControllerParameter parameter in anim.parameters)
        {
            if (parameter.type == AnimatorControllerParameterType.Trigger)
            {
                if (parameter.name.ToLower().Contains("punch") || parameter.name.ToLower().Contains("kick") ||
                    parameter.name.Contains("1") || parameter.name.Contains("2") || parameter.name.Contains("3") || parameter.name.Contains("4"))
                {
                    anim.ResetTrigger(parameter.name);
                }
            }
        }
    }

    public void EndWaveDash()
    {
        playerState.SetState(PLAYER_STATE.WAVEDASH, false);
    }

    public void EndSidestepStartup()
    {
        playerState.SetState(PLAYER_STATE.SIDESTEP_STARTUP, false);
    }

    public void BeginSidestepBackground()
    {
        anim.SetBool("Sidestep_Background", false);
        playerState.SetState(PLAYER_STATE.SIDESTEPPING, true);
    }

    public void BeginSidestepForeground()
    {
        anim.SetBool("Sidestep_Foreground", false);
        playerState.SetState(PLAYER_STATE.SIDESTEPPING, true);
    }

    public void EndSidestep()
    {
        anim.SetBool("Sidestep_Background", false);
        anim.SetBool("Sidestep_Foreground", false);
        playerState.SetState(PLAYER_STATE.SIDESTEPPING, false);
    }

    public void KnockedDown()
    {
        if (!playerState.GetState(PLAYER_STATE.KNOCKED_DOWN))
            playerState.SetState(PLAYER_STATE.KNOCKED_DOWN, true);
    }

    public void FinishGetUp()
    {
        playerState.SetState(PLAYER_STATE.GETUP, false);
        playerState.SetState(PLAYER_STATE.KNOCKED_DOWN, false);
    }

    public bool WallCollisionLeft()
    {
        return playerState.GetState(PLAYER_STATE.WALL_COLLISION_LEFT);
    }

    public void AttackMove(string _hitbox)
    {
        AttackMovement moveParams = GetComponent<HitBoxManager>().GetAttackMoveParams(_hitbox);
        StartCoroutine(Push(moveParams.force, moveParams.length));
    }

    IEnumerator Push(Vector2 _force, int _lengthInFrames)
    {
        int startFrame = Time.frameCount;
        if (!IsFacingRight())
            _force.x *= -1;
        while (startFrame >= Time.frameCount - _lengthInFrames)
        {
            if (playerState.GetState(PLAYER_STATE.WALL_COLLISION_LEFT) || playerState.GetState(PLAYER_STATE.WALL_COLLISION_RIGHT) || playerState.GetState(PLAYER_STATE.OPPONENT_WALL_COLLISION)) _force.x = 0;
            transform.Translate(_force * Time.deltaTime);
            yield return null;
        }
    }
}