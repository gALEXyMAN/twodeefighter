﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{
    // [SerializeField] float maxCameraZoom = 20.0f;
    // [SerializeField] float minCameraZoom = 5.0f;
    [SerializeField] float groundPos = 6.0f;
    GameObject playerOne = null;
    GameObject playerTwo = null;
    Camera cam;
    public float distanceBetweenPlayers;

    // Start is called before the first frame update
    void Start()
    {
        playerOne = GameObject.FindGameObjectWithTag("PlayerOne");
        playerTwo = GameObject.FindGameObjectWithTag("PlayerTwo");
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerOne == null) Debug.LogError("Forgot to tag PlayerOne");
        if (playerTwo == null) Debug.LogError("Forgot to tag PlayerTwo");

        distanceBetweenPlayers = Vector2.Distance(playerOne.transform.position, playerTwo.transform.position);
        if (distanceBetweenPlayers < 20.0f && distanceBetweenPlayers > 12.0f)
        {
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, distanceBetweenPlayers / 12.0f * 5.0f, Time.deltaTime);
        }

        // Position camera in between the two players
        Vector3 pos = cam.transform.position;
        pos.x = Mathf.Min(playerOne.transform.position.x, playerTwo.transform.position.x) + distanceBetweenPlayers / 2.0f;
        pos.y = cam.orthographicSize - groundPos > 0.0f ? cam.orthographicSize - groundPos : 0.0f;
        cam.transform.position = pos;
    }
}
