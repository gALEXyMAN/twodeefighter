using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour
{
    [SerializeField] float forwardDashAmount = 10.0f;
    [SerializeField] float backDashAmount = 20.0f;
    [Range(0, 20)] [SerializeField] int forwardDashLength = 13;
    [Range(0, 20)] [SerializeField] int backDashLength = 13;
    [Range(1, 20)] [SerializeField] int doubleTapWindow = 10;
    [SerializeField] AnimationCurve dashCurve;
    [SerializeField] AnimationCurve backDashCurve;
    int inputFrame = 0;
    int dashLeftCount = 0;
    int dashRightCount = 0;
    bool leftDash = false;
    bool rightDash = false;
    int endFrame = 0;
    Animator anim;
    const string forwardDashTrigger = "ForwardDash";
    const string backDashTrigger = "BackDash";
    bool wallCollisionLeft = false;
    bool wallCollisionRight = false;

    public float ForwardDashAmount { get { return forwardDashAmount; } }

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void UpdateDash(ref PlayerState _state, InputState _input)
    {
        wallCollisionLeft = _state.GetState(PLAYER_STATE.WALL_COLLISION_LEFT);
        wallCollisionRight = _state.GetState(PLAYER_STATE.WALL_COLLISION_RIGHT);

        if (_state.GetState(PLAYER_STATE.CROUCHDASH) || _state.GetState(PLAYER_STATE.KNOCKED_DOWN)) return;

        _state.SetState(PLAYER_STATE.DASHING, leftDash || rightDash ? true : false);

        if (!leftDash && !rightDash)
        {
            LeftInput(_state, _input);
            RightInput(_state, _input);
        }

        if (leftDash)
        {
            DashLeft(_state);
        }
        else if (rightDash)
        {
            DashRight(_state);
        }
    }

    void LeftInput(PlayerState _state, InputState _input)
    {
        int dashLength = _state.FacingRight == true ? backDashLength : forwardDashLength;
        if (_input.GetInput(INPUT.LEFT))
        {
            if (dashLeftCount == 0)
            {
                dashLeftCount++;
                inputFrame = Time.frameCount;
            }
            else if (dashLeftCount == 2)
            {
                if (inputFrame < Time.frameCount - doubleTapWindow
                    || _state.GetState(PLAYER_STATE.ATTACKING))
                {
                    dashLeftCount = 0;
                    return;
                }
                dashLeftCount++;
                leftDash = true;
                if (_state.FacingRight) anim.SetTrigger(backDashTrigger);
                else anim.SetTrigger(forwardDashTrigger);
                endFrame = Time.frameCount + dashLength;
            }
        }
        else if (_input.GetInput(INPUT.LEFT) == false)
        {
            if (dashLeftCount == 1)
            {
                dashLeftCount++;
            }
        }
    }

    void RightInput(PlayerState _state, InputState _input)
    {
        int dashLength = _state.FacingRight == true ? forwardDashLength : backDashLength;
        if (_input.GetInput(INPUT.RIGHT))
        {
            if (dashRightCount == 0)
            {
                dashRightCount++;
                inputFrame = Time.frameCount;
            }
            else if (dashRightCount == 2)
            {
                if (inputFrame < Time.frameCount - doubleTapWindow
                    || _state.GetState(PLAYER_STATE.ATTACKING))
                {
                    dashRightCount = 0;
                    return;
                }
                dashRightCount++;
                rightDash = true;
                endFrame = Time.frameCount + dashLength;
                if (_state.FacingRight) anim.SetTrigger(forwardDashTrigger);
                else anim.SetTrigger(backDashTrigger);
            }
        }
        else if (_input.GetInput(INPUT.RIGHT) == false)
        {
            if (dashRightCount == 1)
            {
                dashRightCount++;
            }
        }
    }

    void DashLeft(PlayerState _state)
    {
        float dashAmount = _state.FacingRight == true ? backDashAmount : forwardDashAmount;
        if (Time.frameCount < endFrame)
        {
            if ((!wallCollisionLeft && _state.FacingRight) ||
                (!wallCollisionRight && !_state.FacingRight))
            {
                if (!_state.GetState(PLAYER_STATE.OPPONENT_WALL_COLLISION))
                {
                    Vector2 pos = transform.position;
                    pos.x -= dashAmount * Time.deltaTime;
                    transform.position = pos;
                }
            }
        }
        else
        {
            leftDash = false;
            dashLeftCount = 0;
            _state.SetState(PLAYER_STATE.OPPONENT_WALL_COLLISION, false);
        }
    }

    void DashRight(PlayerState _state)
    {
        float dashAmount = _state.FacingRight == true ? forwardDashAmount : backDashAmount;
        if (Time.frameCount < endFrame)
        {
            if ((!wallCollisionRight && _state.FacingRight) ||
                (!wallCollisionLeft && !_state.FacingRight))
            {
                if (!_state.GetState(PLAYER_STATE.OPPONENT_WALL_COLLISION))
                {
                    Vector2 pos = transform.position;
                    pos.x += dashAmount * Time.deltaTime;
                    transform.position = pos;
                }
            }
        }
        else
        {
            rightDash = false;
            dashRightCount = 0;
            _state.SetState(PLAYER_STATE.OPPONENT_WALL_COLLISION, false);
        }
    }

    public void PushBack(bool _facingRight, float _pushBackAmount)
    {
        if (_facingRight)
        {
            if (!wallCollisionLeft)
            {
                Vector2 pos = transform.position;
                pos.x -= _pushBackAmount * Time.deltaTime;
                transform.position = pos;
            }
        }
        else
        {
            if (!wallCollisionRight)
            {
                Vector2 pos = transform.position;
                pos.x += _pushBackAmount * Time.deltaTime;
                transform.position = pos;
            }
        }
    }

    public void CancelDash()
    {
        leftDash = false;
        dashLeftCount = 0;
        rightDash = false;
        dashRightCount = 0;
    }
}