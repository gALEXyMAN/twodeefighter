using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    [SerializeField] private float jumpSpeed = 5.0f;
    [SerializeField] private float jumpHorizontalSpeed = 5.0f;
    [SerializeField] private float fallSpeed = 5.0f;
    [SerializeField] private float jumpDeceleration = 0.1f;
    [SerializeField] private float fallAcceleration = 0.1f;

    float currentJumpSpeed = 5.0f;
    private float currentFallSpeed = 5.0f;
    bool jumpRight = false;
    bool jumpLeft = false;

    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void SetJumpLeft(bool _value)
    {
        jumpLeft = _value;
    }

    public void SetJumpRight(bool _value)
    {
        jumpRight = _value;
    }

    public IEnumerator ActivateJump(PlayerState _state)
    {
        currentJumpSpeed = jumpSpeed;
        anim.SetBool("Grounded", false);
        anim.SetBool("Jumping", true);

        Vector2 pos = transform.position;
        if (_state.GetState(PLAYER_STATE.JUMPING))
        {
            while (currentJumpSpeed > 0.0f)
            {
                pos = transform.position;
                pos.y += currentJumpSpeed * Time.deltaTime;
                currentJumpSpeed -= jumpDeceleration;
                if (jumpLeft)
                {
                    pos.x -= jumpHorizontalSpeed * Time.deltaTime;
                }
                else if (jumpRight)
                {
                    pos.x += jumpHorizontalSpeed * Time.deltaTime;
                }
                transform.position = pos;

                yield return null;
            }
        }

        _state.SetState(PLAYER_STATE.JUMPING, false);
        _state.SetState(PLAYER_STATE.FALLING, true);
        anim.SetBool("Jumping", false);
        anim.SetBool("Falling", true);
        currentFallSpeed = fallSpeed;

        if (_state.GetState(PLAYER_STATE.FALLING))
        {
            while (transform.position.y > -1.0f)
            {
                pos = transform.position;
                pos.y -= currentFallSpeed * Time.deltaTime;
                currentFallSpeed += fallAcceleration;

                if (jumpLeft)
                {
                    pos.x -= jumpHorizontalSpeed * Time.deltaTime;
                }
                else if (jumpRight)
                {
                    pos.x += jumpHorizontalSpeed * Time.deltaTime;
                }
                transform.position = pos;

                yield return null;
            }
        }

        pos = transform.position;
        pos.y = -1.0f;
        transform.position = pos;
        _state.SetState(PLAYER_STATE.FALLING, false);
        jumpLeft = false;
        jumpRight = false;
        anim.SetBool("Falling", false);
        anim.SetBool("Grounded", true);
    }

    public IEnumerator Fall(PlayerState _state)
    {
        Vector2 pos;
        if (_state.GetState(PLAYER_STATE.FALLING))
        {
            while (transform.position.y > -1.0f)
            {
                pos = transform.position;
                pos.y -= currentFallSpeed * Time.deltaTime;
                currentFallSpeed += fallAcceleration;

                if (jumpLeft)
                {
                    pos.x -= jumpHorizontalSpeed * Time.deltaTime;
                }
                else if (jumpRight)
                {
                    pos.x += jumpHorizontalSpeed * Time.deltaTime;
                }
                transform.position = pos;

                yield return null;
            }
        }
        pos = transform.position;
        pos.y = -1.0f;
        transform.position = pos;
        _state.SetState(PLAYER_STATE.FALLING, false);
        jumpLeft = false;
        jumpRight = false;
        anim.SetBool("Falling", false);
        anim.SetBool("Grounded", true);
    }
}