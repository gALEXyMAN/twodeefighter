﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    bool holdingButton = false;
    bool[] previousState = new bool[(int)INPUT.MAX_INPUT];
    CommandList commandList;

    void Start()
    {
        commandList = GetComponent<CommandList>();
    }

    public void UpdateAttack(ref PlayerState _state, InputState _input)
    {
        if(_state.GetState(PLAYER_STATE.HIT)) return;

        bool[] currentState = _input.GetCurrentState();

        bool leftPunch = _input.GetInput(INPUT.LEFT_PUNCH);
        bool leftKick = _input.GetInput(INPUT.LEFT_KICK);
        bool rightPunch = _input.GetInput(INPUT.RIGHT_PUNCH);
        bool rightKick = _input.GetInput(INPUT.RIGHT_KICK);

        if (!holdingButton)
        {
            //if (!_state.GetState(PLAYER_STATE.ATTACKING))
            {
                if (leftPunch && leftKick && rightPunch && rightKick)
                {
                    commandList.KiCharge(_state, _input);
                    holdingButton = true;
                    System.Array.Copy(currentState, previousState, currentState.Length);
                    //_state.SetState(PLAYER_STATE.ATTACKING, true);
                }
                else if (leftPunch && rightPunch)
                {
                    commandList.LeftAndRightPunchCommands(_state, _input);
                    holdingButton = true;
                    System.Array.Copy(currentState, previousState, currentState.Length);
                    _state.SetState(PLAYER_STATE.ATTACKING, true);
                }
                else if (leftPunch && leftKick)
                {
                    commandList.LeftPunchAndKickCommands(_state, _input);
                    holdingButton = true;
                    System.Array.Copy(currentState, previousState, currentState.Length);
                    //_state.SetState(PLAYER_STATE.ATTACKING, true);
                }
                else if (leftPunch && rightKick)
                {
                    holdingButton = true;
                    System.Array.Copy(currentState, previousState, currentState.Length);
                    //_state.SetState(PLAYER_STATE.ATTACKING, true);
                }
                else if (rightPunch && leftKick)
                {
                    holdingButton = true;
                    System.Array.Copy(currentState, previousState, currentState.Length);
                    //_state.SetState(PLAYER_STATE.ATTACKING, true);
                }
                else if (rightPunch && rightKick)
                {
                    commandList.RightPunchAndKickCommands(_state, _input);
                    holdingButton = true;
                    System.Array.Copy(currentState, previousState, currentState.Length);
                    //_state.SetState(PLAYER_STATE.ATTACKING, true);
                }
                else if (leftKick && rightKick)
                {
                    commandList.LeftAndRightKickCommands(_state, _input);
                    holdingButton = true;
                    System.Array.Copy(currentState, previousState, currentState.Length);
                    //_state.SetState(PLAYER_STATE.ATTACKING, true);
                }
                else if (leftPunch)
                {
                    commandList.LeftPunchCommands(_state, _input);
                    holdingButton = true;
                    System.Array.Copy(currentState, previousState, currentState.Length);
                    _state.SetState(PLAYER_STATE.ATTACKING, true);
                }
                else if (rightPunch)
                {
                    commandList.RightPunchCommands(_state, _input);
                    holdingButton = true;
                    System.Array.Copy(currentState, previousState, currentState.Length);
                    _state.SetState(PLAYER_STATE.ATTACKING, true);
                }
                else if (leftKick)
                {
                    commandList.LeftKickCommands(_state, _input);
                    holdingButton = true;
                    System.Array.Copy(currentState, previousState, currentState.Length);
                    _state.SetState(PLAYER_STATE.ATTACKING, true);
                }
                else if (rightKick)
                {
                    commandList.RightKickCommands(_state, _input);
                    holdingButton = true;
                    System.Array.Copy(currentState, previousState, currentState.Length);
                    _state.SetState(PLAYER_STATE.ATTACKING, true);
                }
            }
        }
        else
        {
            for (int i = 0; i < (int)INPUT.MAX_INPUT; ++i)
            {
                if (previousState[i] != currentState[i])
                {
                    holdingButton = false;
                    return;
                }
            }
        }
    }
}
