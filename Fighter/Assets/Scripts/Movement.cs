using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float m_movementSpeed = 5.0f;

    Animator anim;
    Dash dash;
    Jump jumpComponent;
    TechRoll techRoll;
    const string walkBack = "WalkBack";
    const string walkForward = "WalkForward";
    const string crouch = "Crouch";
    bool sidestepBackground = false;
    bool sidestepForeground = false;
    bool wallCollisionLeft = false;
    bool wallCollisionRight = false;

    public float MovementSpeed { get { return m_movementSpeed; } }

    void Start()
    {
        anim = GetComponent<Animator>();
        jumpComponent = GetComponent<Jump>();
        dash = GetComponent<Dash>();
        techRoll = GetComponent<TechRoll>();
    }

    public void UpdateMovement(ref PlayerState _state, InputState _input)
    {
        wallCollisionLeft = _state.GetState(PLAYER_STATE.WALL_COLLISION_LEFT);
        wallCollisionRight = _state.GetState(PLAYER_STATE.WALL_COLLISION_RIGHT);

        if (_state.GetState(PLAYER_STATE.JUMPING))
        {
            if (_input.GetInput(INPUT.LEFT))
            {
                jumpComponent.SetJumpLeft(true);
            }
            else if (_input.GetInput(INPUT.RIGHT))
            {
                jumpComponent.SetJumpRight(true);
            }
            return;
        }

        if (_state.GetState(PLAYER_STATE.ATTACKING) ||
            _state.GetState(PLAYER_STATE.FALLING))
        {
            anim.SetBool(walkBack, false);
            anim.SetBool(walkForward, false);
            //anim.SetBool(crouch, false);
            return;
        }

        bool immobile = _state.GetState(PLAYER_STATE.CROUCHING) || _state.GetState(PLAYER_STATE.SIDESTEPPING)
                        || _state.GetState(PLAYER_STATE.SIDESTEP_STARTUP) || _state.GetState(PLAYER_STATE.DASHING)
                        || _state.GetState(PLAYER_STATE.HIT);

        if (_input.GetInput(INPUT.LEFT) && !immobile)
        {
            LeftInput(_state);
        }
        else if (_input.GetInput(INPUT.RIGHT) && !immobile)
        {
            RightInput(_state);
        }
        else
        {
            anim.SetBool(walkBack, false);
            anim.SetBool(walkForward, false);
            _state.SetState(PLAYER_STATE.MOVING, false);
        }

        UpInput(_state, _input);
        DownInput(_state, _input);
    }

    void LeftInput(PlayerState _state)
    {
        if (_state.GetState(PLAYER_STATE.KNOCKED_DOWN))
        {
            techRoll.TechRollLeft(_state);
        }
        else
        {
            // When player is facing left, wall colliders are flipped
            MoveLeft(_state);
            _state.SetState(PLAYER_STATE.MOVING, true);
            anim.SetBool(walkBack, true);
            anim.SetBool(crouch, false);
        }
    }

    void RightInput(PlayerState _state)
    {
        if (_state.GetState(PLAYER_STATE.KNOCKED_DOWN))
        {
            techRoll.TechRollRight(_state);
        }
        else
        {
            MoveRight(_state);
            _state.SetState(PLAYER_STATE.MOVING, true);
            anim.SetBool(walkForward, true);
            anim.SetBool(crouch, false);
        }
    }

    void UpInput(PlayerState _state, InputState _input)
    {
        if (_input.GetInput(INPUT.UP) == true)
        {
            if (_state.GetState(PLAYER_STATE.KNOCKED_DOWN))
            {
                if (!_state.GetState(PLAYER_STATE.GETUP))
                {
                    anim.SetTrigger("GetUp");
                    _state.SetState(PLAYER_STATE.GETUP, true);
                }
            }
            else if (!_state.GetState(PLAYER_STATE.SIDESTEPPING))
            {
                if (sidestepBackground)
                {
                    anim.SetBool("Jumping", true);
                    if (_state.GetState(PLAYER_STATE.SIDESTEP_STARTUP) == false)
                    {
                        _state.SetState(PLAYER_STATE.JUMPING, true);
                        StartCoroutine(jumpComponent.ActivateJump(_state));
                        sidestepBackground = false;
                    }
                }
                else
                {
                    _state.SetState(PLAYER_STATE.SIDESTEP_STARTUP, true);
                    sidestepBackground = true;
                    anim.SetTrigger("Sidestep");
                }
            }
        }
        else if (_input.GetInput(INPUT.UP) == false)
        {
            if (sidestepBackground)
            {
                sidestepBackground = false;
                anim.SetBool("Sidestep_Background", true);
                anim.SetBool("Jumping", false);
                dash.CancelDash();
            }
        }
    }

    void DownInput(PlayerState _state, InputState _input)
    {
        if (_input.GetInput(INPUT.DOWN) == true)
        {
            if (!_state.GetState(PLAYER_STATE.KNOCKED_DOWN) && !_state.GetState(PLAYER_STATE.SIDESTEPPING))
            {
                if (_state.GetState(PLAYER_STATE.SIDESTEP_STARTUP))
                {
                    _state.SetState(PLAYER_STATE.CROUCHING, true);
                    anim.SetBool(walkBack, false);
                    anim.SetBool(walkForward, false);
                    anim.SetBool(crouch, true);
                }
                else
                {
                    if (!_state.GetState(PLAYER_STATE.CROUCHING))
                    {
                        _state.SetState(PLAYER_STATE.SIDESTEP_STARTUP, true);
                        anim.SetTrigger("Sidestep");
                        sidestepForeground = true;
                    }
                }
            }
        }
        else if (_input.GetInput(INPUT.DOWN) == false)
        {
            if (_state.GetState(PLAYER_STATE.SIDESTEP_STARTUP) && sidestepForeground)
            {
                anim.SetBool("Sidestep_Foreground", true);
                sidestepForeground = false;
                dash.CancelDash();
            }
            _state.SetState(PLAYER_STATE.CROUCHING, false);
            anim.SetBool(crouch, false);
        }
    }

    public void PushBack(bool _facingRight, float _pushbackAmount)
    {
        if (_facingRight)
        {
            if (!wallCollisionLeft)
            {
                Vector2 pos = transform.position;
                pos.x -= _pushbackAmount * Time.deltaTime;
                transform.position = pos;
            }
        }
        else
        {
            if (!wallCollisionRight)
            {
                Vector2 pos = transform.position;
                pos.x += _pushbackAmount * Time.deltaTime;
                transform.position = pos;
            }
        }
    }

    void MoveLeft(PlayerState _state)
    {

        // When player is facing left, wall colliders are flipped
        bool wallCollision = (wallCollisionLeft && _state.FacingRight) || (wallCollisionRight && !_state.FacingRight) || (_state.GetState(PLAYER_STATE.OPPONENT_WALL_COLLISION) && !_state.FacingRight);
        if (wallCollision == false)
        {
            Vector2 pos = transform.position;
            pos.x -= m_movementSpeed * Time.deltaTime;
            transform.position = pos;
            _state.SetState(PLAYER_STATE.OPPONENT_WALL_COLLISION, false);
        }
    }

    void MoveRight(PlayerState _state)
    {
        bool wallCollision = (wallCollisionRight && _state.FacingRight) || (wallCollisionLeft && !_state.FacingRight) || (_state.GetState(PLAYER_STATE.OPPONENT_WALL_COLLISION) && _state.FacingRight);
        if (wallCollision == false)
        {
            Vector2 pos = transform.position;
            pos.x += m_movementSpeed * Time.deltaTime;
            transform.position = pos;
            _state.SetState(PLAYER_STATE.OPPONENT_WALL_COLLISION, false);
        }
    }
}