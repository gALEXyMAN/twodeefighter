﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementCollision : MonoBehaviour
{
    // Push box
    [SerializeField] BoxCollider2D boxCollider = null;
    string playerTag = "PlayerOne";
    float forwardDashAmount = 0.0f;
    float movementSpeed = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        playerTag = tag;
        forwardDashAmount = GetComponent<Dash>().ForwardDashAmount;
        movementSpeed = GetComponent<Movement>().MovementSpeed;
    }

    // Update is called once per frame
    public void UpdateMovementCollision(PlayerState _state)
    {
        Vector2 pos = boxCollider.transform.position;
        pos += boxCollider.offset;
        Collider2D[] hits = Physics2D.OverlapBoxAll(pos, boxCollider.size, 0);
        foreach (Collider2D hit in hits)
        {
            if (hit == boxCollider)
                continue;

            ColliderDistance2D colliderDistance = hit.Distance(boxCollider);

            if (tag == "PlayerOne" && hit.tag == "HurtboxP2" ||
                tag == "PlayerTwo" && hit.tag == "HurtboxP1")
            {
                if (colliderDistance.isOverlapped)
                {
                    Debug.DrawLine(colliderDistance.pointA, colliderDistance.pointB, Color.red);
                    bool wallCollisionLeft = hit.GetComponentInParent<Player>().WallCollisionLeft();

                    _state.SetState(PLAYER_STATE.OPPONENT_WALL_COLLISION, wallCollisionLeft);

                    if (wallCollisionLeft || _state.GetState(PLAYER_STATE.WALL_COLLISION_LEFT) || _state.GetState(PLAYER_STATE.WALL_COLLISION_RIGHT))
                        return;

                    if (_state.GetState(PLAYER_STATE.DASHING) || _state.GetState(PLAYER_STATE.WAVEDASH))
                    {
                        hit.GetComponentInParent<Dash>().PushBack(!_state.FacingRight, forwardDashAmount);
                        return;
                    }
                    if (_state.GetState(PLAYER_STATE.MOVING))
                    {
                        hit.GetComponentInParent<Movement>().PushBack(!_state.FacingRight, movementSpeed);
                        return;
                    }
                    Vector2 pointB = colliderDistance.pointB;
                    Vector2 pointA = colliderDistance.pointA;
                    pointB.y = 0.0f;    // Prevent vertical translation
                    pointA.y = 0.0f;
                    transform.Translate((pointA - pointB));
                }
            }
        }
    }
}
