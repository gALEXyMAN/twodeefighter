﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitStun : MonoBehaviour
{
    [SerializeField] float fallSpeed = 1.0f;
    [Range(0.1f, 1.0f)] [SerializeField] float fallAcceleration = 1.0f;
    Animator anim;
    bool launched = false;
    const string hitHighTrigger = "Hit_High", hitMidTrigger = "Hit_Mid", hitLowTrigger = "Hit_Low", hitCrouchTrigger = "Hit_Crouch", launchedTrigger = "Launched";
    const string blockHighTrigger = "Block_High", blockCrouchTrigger = "Block_Crouch";
    PlayerState state;
    float bounceForce = 50.0f;
    Health health;
    int juggleCount = 0;
    float juggleMultiplier = 1.0f;

    public void SetState(PlayerState _state)
    {
        state = _state;
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        health = GetComponent<Health>();
    }

    void SetJuggleMultiplier()
    {
        switch (juggleCount)
        {
            case 0:
                juggleMultiplier = 1.0f;
                break;
            case 1:
                juggleMultiplier = 1.0f;
                break;
            case 2:
                juggleMultiplier = 0.7f;
                break;
            case 3:
                juggleMultiplier = 0.5f;
                break;
            case 4:
                juggleMultiplier = 0.4f;
                break;
            default:
                juggleMultiplier = 0.3f;
                break;
        }
    }

    public void HitHigh(Vector2 _force, int _length, int _damage)
    {
        state.SetState(PLAYER_STATE.HIT, true);
        if (state.GetState(PLAYER_STATE.BLOCKING))
        {
            anim.SetTrigger(blockHighTrigger);
        }
        else if (state.GetState(PLAYER_STATE.FALLING))
        {
            juggleCount++;
            SetJuggleMultiplier();
            _force.y += bounceForce;
            Launched(_force, _length, _damage);
        }
        else
        {
            anim.SetTrigger(hitHighTrigger);
            Vector2 force = _force;
            if (state.FacingRight)
                force.x = -force.x;

            PushBack(force, _length);
            int scaledDamage = Mathf.FloorToInt(_damage * juggleMultiplier);
            health.Damage(scaledDamage);
        }
    }

    public void HitMid(Vector2 _force, int _length, int _damage)
    {
        state.SetState(PLAYER_STATE.HIT, true);
        if (state.GetState(PLAYER_STATE.BLOCKING))
        {
            anim.SetTrigger(blockHighTrigger);
        }
        else if (state.GetState(PLAYER_STATE.FALLING))
        {
            juggleCount++;
            SetJuggleMultiplier();
            _force.y += bounceForce;
            Launched(_force, _length, _damage);
        }
        else
        {
            anim.SetTrigger(hitMidTrigger);
            Vector2 force = _force;
            if (state.FacingRight)
                force.x = -force.x;

            PushBack(force, _length);
            int scaledDamage = Mathf.FloorToInt(_damage * juggleMultiplier);
            health.Damage(scaledDamage);
        }
    }

    public void HitLow(Vector2 _force, int _length, int _damage)
    {
        state.SetState(PLAYER_STATE.HIT, true);
        if (state.GetState(PLAYER_STATE.BLOCKING) && state.GetState(PLAYER_STATE.CROUCHING))
        {
            anim.SetTrigger(blockCrouchTrigger);
        }
        else if (state.GetState(PLAYER_STATE.FALLING))
        {
            juggleCount++;
            SetJuggleMultiplier();
            _force.y += bounceForce;
            Launched(_force, _length, _damage);
        }
        else
        {
            if (state.GetState(PLAYER_STATE.CROUCHING))
            {
                anim.SetTrigger(hitCrouchTrigger);
            }
            else if (!state.GetState(PLAYER_STATE.KNOCKED_DOWN))
            {
                anim.SetTrigger(hitLowTrigger);
            }
            Vector2 force = _force;
            if (state.FacingRight)
                force.x = -force.x;

            PushBack(force, _length);
            int scaledDamage = Mathf.FloorToInt(_damage * juggleMultiplier);
            health.Damage(scaledDamage);
        }
    }

    public void Launched(Vector2 _force, int _length, int _damage)
    {
        state.SetState(PLAYER_STATE.HIT, true);
        if (state.GetState(PLAYER_STATE.BLOCKING))
        {
            anim.SetTrigger(blockHighTrigger);
        }
        else
        {
            juggleCount++;
            SetJuggleMultiplier();
            int scaledDamage = Mathf.FloorToInt(_damage * juggleMultiplier);

            launched = true;
            anim.SetTrigger(launchedTrigger);
            state.SetState(PLAYER_STATE.FALLING, true);
            anim.SetBool("Grounded", false);
            Vector2 force = _force;
            if (state.FacingRight)
                force.x = -force.x;

            PushBack(force, _length, true);
            health.Damage(scaledDamage);
        }
    }

    public void PushBack(Vector2 _force, int _lengthInFrames, bool _launch = false)
    {
        StartCoroutine(Push(_force, _lengthInFrames, _launch));
    }

    IEnumerator Push(Vector2 _force, int _lengthInFrames, bool _launch = false)
    {
        int startFrame = Time.frameCount;
        while (startFrame >= Time.frameCount - _lengthInFrames)
        {
            if (state.GetState(PLAYER_STATE.WALL_COLLISION_LEFT) || state.GetState(PLAYER_STATE.WALL_COLLISION_RIGHT)) _force.x = 0;
            transform.Translate(_force * Time.deltaTime);
            yield return null;
        }
        if (_launch)
            yield return Fall();
    }

    IEnumerator Fall()
    {
        launched = false;
        Vector2 pos;
        float currentFallSpeed = fallSpeed;
        if (state.GetState(PLAYER_STATE.FALLING))
        {
            while (transform.position.y > -1.0f)
            {
                if (launched)
                    break;
                pos = transform.position;
                pos.y -= currentFallSpeed * Time.deltaTime;
                currentFallSpeed += fallAcceleration;
                transform.position = pos;
                yield return null;
            }
        }
        if (!launched)
        {
            pos = transform.position;
            pos.y = -1.0f;
            transform.position = pos;
            state.SetState(PLAYER_STATE.FALLING, false);
            state.SetState(PLAYER_STATE.KNOCKED_DOWN, true);
            anim.SetBool("Falling", false);
            anim.SetBool("Grounded", true);
            launched = false;
            juggleCount = 0;
            juggleMultiplier = 1.0f;
        }
    }

    public void EndHitStun()
    {
        state.SetState(PLAYER_STATE.HIT, false);
    }
}
